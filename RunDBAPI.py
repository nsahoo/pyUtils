#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   RunDBAPI.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   30.03.2016
# =============================================================================
"""RunDB HTTP API interface.

Implements the calls detailed in https://lbdokuwiki.cern.ch/rundb:rundb_web_interface_api

Provides:
    - A generic API caller, `do_api_call`, and API JSON output sanitizers
     (`sanitize_json` and `sanitize_decorator`).
    - Specific calls to the API.
    - Exceptions (`RequestErrot` and `WrongContent`).

"""

import urlparse
import datetime
from functools import wraps

import requests


# API URL
API_URL = 'http://lbrundb.cern.ch/api/'


# Exception handlers
class RequestError(requests.exceptions.RequestException):
    """Generic error in HTTP requests.

    Inherits from `requests.exceptions.RequestException`.

    """


class WrongContent(requests.exceptions.RequestException):
    """The response has the wrong content.

    Inherits from `requests.exceptions.RequestException`.

    """


# Debug function
def set_debug_logging():
    """Turns on debug logging for urllib calls."""
    import logging
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True


# Improve formatting of JSON responses
def sanitize_json(json_data):
    """Format certain fields from API responses.

    Conversions from `unicode` outputs are:
        - ('*time', 'timestamp') -> `datetime.datetime`.
        - ('*Rate', '*Lumi', 'av*') -> `float`
        - ('n*Bunches*') -> `int`
        - 'tck' -> hex representation.
            TODO: What to do with negative TCKs?

    Arguments:
        json_data (dict): Data to sanitize.

    Returns:
        dict: Sanitized JSON data.

    """
    for field in json_data:
        if isinstance(json_data[field], unicode):
            if field.endswith('time'):
                json_data[field] = datetime.datetime.strptime(json_data[field],
                                                              '%Y-%m-%dT%H:%M:%S')
            elif field == 'timestamp':
                json_data[field] = datetime.datetime.strptime(json_data[field],
                                                              '%Y-%m-%dT%H:%M:%Sz')
            elif field.endswith('Rate') or field.endswith('Lumi') or field.startswith('av'):
                json_data[field] = float(json_data[field])
            elif 'Bunches' in field and field.startswith('n'):
                json_data[field] = int(json_data[field])
            elif field.lower() == 'tck':
                if json_data[field]:
                    try:
                        json_data[field] = '0x{0:08X}'.format(int(json_data[field]))
                    except ValueError:
                        # Typically when the TCK field is already properly
                        # formatted
                        pass
    return json_data


def sanitize_decorator(func):
    """Sanitize the JSON output of API responses."""
    @wraps(func)
    def output_wrapper(*args, **kwargs):
        """Format certain fields from API responses.

        Uses `sanitize_json` internally.

        """
        return sanitize_json(func(*args, **kwargs))
    return output_wrapper


def tck_to_str(tck):
    """Get TCK in string format.

    Arguments:
        tck (int, str): TCK value.

    Returns:
        str: TCK in string format.

    """
    if isinstance(tck, str):
        tck = int(tck, 16)
    return '0x{0:08X}'.format(tck)


# Calls to the API
def do_api_call(method, **kwargs):
    """Perform an API call to the RunDB HTTP Interface.

    Hides all network calls and error handling.

    Arguments:
        method (str): API method to poll.
        **kwargs (dict): Keyword arguments for API call.

    Returns:
        dict: JSON-ified API response.

    Raises:
        RequestError: When the network calls fail. Precise nature of the
            error is indicated in the exception message.
        WrongContent: When the response from the API is not well formed.

    """
    headers = {'content-type': 'application/json'}
    url_to_get = urlparse.urljoin(API_URL, method)
    try:
        response = requests.get(url_to_get,
                                params=kwargs,
                                headers=headers,
                                timeout=(10.0, 20.0))
        response.raise_for_status()
    except requests.exceptions.ConnectionError:
        raise RequestError('DNS lookup error when connecting to %s' % url_to_get)
    except requests.exceptions.ReadTimeout:
        raise RequestError('Timeout error when connecting to %s' % url_to_get)
    except requests.exceptions.HTTPError as error:
        raise RequestError('HTTP error -> %s' % error.message)
    if response.headers["content-type"] != 'application/json':
        raise WrongContent('Wrong headers', response=response)
    return response.json()


@sanitize_decorator
def get_run_info(run_id):
    """Get run information.

    Arguments:
        run_id (int): Run to get info from.

    Returns:
        dict: Run information.

    Raises:
        Whatever `do_api_call` raises in case of failure.

    Example:
        >> print get_run_info(103515)
        {u'LHCState': u'PHYSICS',
        u'avHltPhysRate': 3205.3882852703,
        u'avL0PhysRate': 816732.75292916,
        u'avLumi': 359.24,
        u'avMu': 1.4500368323385,
        u'avPhysDeadTime': 3.6540410003456,
        u'beamenergy': 3500.0,
        u'betaStar': u'3',
        u'destination': u'OFFLINE',
        u'endLumi': 1061110.0,
        u'endtime': datetime.datetime(2011, 10, 15, 20, 48, 56),
        u'fillid': 2216,
        u'magnetCurrent': u'-5850',
        u'magnetState': u'DOWN',
        u'next_runid': 103523,
        u'partitionid': 32767,
        u'partitionname': u'LHCb',
        u'prev_runid': 103514,
        u'program': u'Moore',
        u'programVersion': u'v12r9p1',
        u'runid': 103515,
        u'runtype': u'COLLISION11',
        u'startLumi': 0.0,
        u'starttime': datetime.datetime(2011, 10, 15, 19, 54, 1),
        u'state': u'IN BKK',
        u'tck': '0x00790038',
        u'veloOpening': u'-0.00045204162597656',
        u'veloPosition': u'Closed'}

    """
    return do_api_call('run/%s' % run_id)


@sanitize_decorator
def get_fill_info(fill_id):
    """Get fill information.

    Arguments:
        fill_id (int): Fill to get info from.

    Returns:
        dict: Run information.

    Raises:
        Whatever `do_api_call` raises in case of failure.

    Example:
        >> print get_fill_info(4720)
        {u'fill_id': 4720,
        u'lumi_hvon': 9.3233,
        u'lumi_logged': 8.0715,
        u'lumi_running': 8.1247,
        u'lumi_total': 9.3354,
        u'lumi_veloin': 9.133,
        u'nBunchesB1': 518,
        u'nBunchesB2': 516,
        u'nCollidingBunches': 24,
        u'peakHltPhysRate': 8224.9283203125,
        u'peakL0PhysRate': 19422.37130654,
        u'peakLumi': 0.021477265731497,
        u'peakMu': u'0.93420475445987',
        u'peakPhysDeadTime': u'100',
        u'prev_fill_id': 4719,
        u'time_hvon': 39523.684,
        u'time_logged': 37369.2078,
        u'time_running': 37602.849,
        u'time_total': 39564.111,
        u'time_veloin': 38872.268,
        u'timestamp': datetime.datetime(2015, 12, 13, 13, 9, 56),
        u'veloLostLumi': 0.062435130291362,
        u'veloLostTime': u'240'}

    """
    return do_api_call('fill/%s' % fill_id)


def search_runs(**criteria):
    """Get the runs matching the provided criteria.

    Note:
        By default, only the last 20 are returned.

    Arguments:
        **criteria: search criteria can be
            - `tck`: filter by TCK.
            - `magnet_polarity`: filter by magnet polarity.
            - `run_type`: filter by type of run.
            - `starttime`: in the format '%Y-%m-%dT%H:%M:%S' or
                `datetime.datetime`.
            - `endtime`: in the format '%Y-%m-%dT%H:%M:%S' or
                `datetime.datetime`.
            - `rows`: number of runs to return, starting with the latest one.
                `rows=-1` returns all the runs that match the criteria.

    Returns:
        dict: Runs matching the search criteria. Keys are:
            - `runs`: list of run objects similar to those returned by
                `get_run_info`.
            - `start`: the first row.
            - `totalResults`: total number of runs matching the query.

    Raises:
        Whatever `do_api_call` raises in case of failure.

    Example:
        >> print search_runs(tck='0x0115014E',
                             magnet_polarity='down',
                             run_type='collision',
                             starttime='2015-11-01T00:00:00',
                             endtime='2015-12-01T00:00:00',
                             rows=1)
        {u'runs': [{u'LHCState': u'PHYSICS',
                    u'avHltPhysRate': 57922.555950203,
                    u'avL0PhysRate': 195282.16154,
                    ...
                    u'triggerConfiguration': u'Physics_5TeV',
                    u'veloOpening': u'-0.00072669982910156',
                    u'veloPosition': u'Closed'}],
        u'start': 0,
        u'totalResults': 129}

    """
    # Sanitize inputs
    for time_var in ['starttime', 'endtime']:
        if time_var in criteria:
            if isinstance(criteria[time_var], datetime.datetime):
                criteria[time_var] = criteria[time_var].strftime(format='%Y-%m-%dT%H:%M:%S')
    if 'tck' in criteria:
        criteria['tck'] = tck_to_str(criteria['tck'])
    # Call and sanitize outputs
    res = do_api_call('search', **criteria)
    res['runs'] = [sanitize_json(run) for run in res['runs']]
    return res


def get_physics_fills(**criteria):
    """Get the last physics fills according to the given criteria.

    Note:
        By default, only the last 20 are returned.

    Arguments:
        **criteria: search criteria can be
            - `starttime`: in the format '%Y-%m-%dT%H:%M:%S' or
                `datetime.datetime`.
            - `endtime`: in the format '%Y-%m-%dT%H:%M:%S' or
                `datetime.datetime`.
            - `rows`: number of fills to return, starting with the latest one.
                `rows=-1` returns all the fills that match the criteria. If
                `starttime` and `endtime` are specified, this option doesn't
                have any effect.

    Returns:
        dict: Fills matching the search criteria. Keys are:
            - `fills`: list of run objects similar to those returned by
            `get_fill_info`.
            - `totalResults`: total number of runs matching the query.

    Raises:
        Whatever `do_api_call` raises in case of failure.

    Example:
        >> print get_physics_fills(starttime='2015-11-01T00:00:00',
                                   endtime='2015-12-01T00:00:00',
                                   rows=1)
        {u'totalResults': 16,
        u'fills': [{u'nCollidingBunches': 24,
                    u'peakPhysDeadTime': u'70.64453125',
                    u'peakMu': u'0.50793482214793',
                    u'time_logged': 21839.981,
                    u'time_hvon': 24782.241,
                    u'veloLostTime': u'240',
                    u'lumi_total': 0.1885,
                    u'fill_id': 4677,
                    u'lumi_hvon': 0.1885,
                    u'peakLumi': 0.0,
                    u'time_veloin': 24439.135,
                    u'peakL0PhysRate': 3544.4029880882,
                    u'timestamp': datetime.datetime(2015, 11, 30, 1, 36, 52),
                    u'lumi_running': 0.0,
                    u'veloLostLumi': 0.0,
                    u'time_running': 21849.906,
                    u'nBunchesB1': 426,
                    u'nBunchesB2': 424,
                    u'lumi_veloin': 0.1885,
                    u'peakHltPhysRate': 15409.740234375,
                    u'lumi_logged': 0.0,
                    u'time_total': 24835.309},
                    ...
                   ]
    """
    # Sanitize inputs
    for time_var in ['starttime', 'endtime']:
        if time_var in criteria:
            if isinstance(criteria[time_var], datetime.datetime):
                criteria[time_var] = criteria[time_var].strftime(format='%Y-%m-%dT%H:%M:%S')
    # Call and sanitize outputs
    res = do_api_call('fills/physics', **criteria)
    res['fills'] = [sanitize_json(run) for run in res['fills']]
    return res


@sanitize_decorator
def get_integrated_lumi(**criteria):
    """Calculate the integrated lumi of the runs matching the provided criteria.

    Arguments:
        **criteria: search criteria can be
            - `tck`: filter by TCK.
            - `magnet_polarity`: filter by magnet polarity.
            - `run_type`: filter by type of run.
            - `starttime`: in the format '%Y-%m-%dT%H:%M:%S' or
                `datetime.datetime`.
            - `endtime`: in the format '%Y-%m-%dT%H:%M:%S' or
                `datetime.datetime`.

    Returns:
        dict: Integrated lumi matching the search criteria. Keys are:
            - `totalLumi`: total luminosity.
            - `totalRuns`: number of runs matching the criteria.
            - `tck`: TCK if specified.

    Raises:
        Whatever `do_api_call` raises in case of failure.

    Example:
        >> print get_integrated_lumi(tck='0x0115014E')
        {u'totalLumi': 8294944.7100436315, u'totalRuns': 143, u'tck': u'0x0115014E'}

    """
    if 'tck' in criteria:
        criteria['tck'] = tck_to_str(criteria['tck'])
    return do_api_call('tck_lumi', **criteria)


@sanitize_decorator
def get_integrated_lumi_upto_run(run_id):
    """Calculate the integrated lumi of an LHC Run up to the given run ID.

    Arguments:
        run_id (int): Run ID up to which lumi is integrated.

    Returns:
        dict: Integrated lumi up to the given run. Keys are:
            - `totalLumi`: total luminosity.
            - `totalRuns`: number of runs matching the criteria.
            - `lhcRun`: run of the LHC for which lumi was integrated (1 or 2).
            - `upToRun`: last run accounted for in the lumi integration.

    Raises:
        Whatever `do_api_call` raises in case of failure.

    Example:
        >> print get_integrated_lumi_upto_run(117098)
        {u'totalLumi': 1467086597.0171585, u'totalRuns': 4597, u'lhcRun': 1, u'upToRun': u'117098'}

    """
    return do_api_call('run/int_lumi/%s' % run_id)


@sanitize_decorator
def get_tck_years(tck):
    """Get years in which a TCK was used.

    Note:
        In this case, the call to the API returns a list because it's possible
        to get the full list of tcks by not passing any parameters to the call.
        In this function, only one TCK is returned.

    Arguments:
        tck (str): TCK to get information for.

    Returns:
        dict: Years in which the TCK was used. Keys are:
            - `tck`: the TCK.
            - `years`: list of years.

    Raises:
        Whatever `do_api_call` raises in case of failure.

    Example:
        >> print get_tck_years
    ('0x0115014E')
        {u'tck': '0x0115014E', u'years': [u'2016', u'2015']}

    """
    return do_api_call('tck_year', tck=tck_to_str(tck))[0]


@sanitize_decorator
def get_tck_moore(tck):
    """Get the Moore versions used with a given TCK.

    Hopefully, only one Moore version should be returned.

    Note:
        In this case, the call to the API returns a list because it's possible
        to get the full list of tcks by not passing any parameters to the call.
        In this function, only one TCK is returned.

    Arguments:
        tck (str): TCK to get information for.

    Returns:
        dict: Years in which the TCK was used. Keys are:
            - `tck`: the TCK.
            - `moore_versions`: list of Moore versions which ran that TCK.

    Raises:
        Whatever `do_api_call` raises in case of failure.

    Example:
        >> print get_tck_moore('0x0115014E')
        {u'moore_versions': [u'v24r3'], u'tck': '0x0115014E'}

    """
    return do_api_call('tck_moore', tck=tck_to_str(tck))[0]

# EOF
